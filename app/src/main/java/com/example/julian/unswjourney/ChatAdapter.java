package com.example.julian.unswjourney;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<chat_rec> {
    private List<ChatMessage> MessageList = null;

    public ChatAdapter(List<ChatMessage> msgDtoList) {
        this.MessageList = msgDtoList;
    }


    @Override
    public chat_rec onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.msglist, parent, false);
        return new chat_rec(view);
    }

    @Override
    public void onBindViewHolder( chat_rec viewHolder, int position) {
        ChatMessage Message = this.MessageList.get(position);
        if (Message.getMsgUser().equals("user")) {
            viewHolder.rightText.setText(Message.getMsgText());
            viewHolder.rightText.setVisibility(LinearLayout.VISIBLE);
            viewHolder.leftText.setVisibility(LinearLayout.GONE);
        }
        else {
            viewHolder.leftText.setText(Message.getMsgText());
            viewHolder.rightText.setVisibility(LinearLayout.GONE);
            viewHolder.leftText.setVisibility(LinearLayout.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if(MessageList==null)
        {
            MessageList = new ArrayList<ChatMessage>();
        }
        return MessageList.size();
    }
}

