package com.example.julian.unswjourney;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class AlertDialog extends Activity  {
    TextView thetitle;
    TextView text;
    private String Title;
    private String Body;
    private String Key;
    CheckBox Remember;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        super.onCreate(savedInstanceState);


        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_alert_dialog_box);
         Remember = (CheckBox) findViewById(R.id.RemindBox);

        TextView thetitle = (TextView) findViewById(R.id.alertybody);
        TextView text = (TextView) findViewById(R.id.AlertText);
        Button leftbutton = (Button) findViewById(R.id.okalertbutton);
        leftbutton.setVisibility(View.INVISIBLE);
        Button rightbutton = (Button)findViewById(R.id.okalertbutton2);
        Title = (String) getIntent().getSerializableExtra("Title");
        text.setText(Title);
        Body = (String) getIntent().getSerializableExtra("Body");
        Key = (String) getIntent().getSerializableExtra("Key");
        ShowCheckbox( Key);
        thetitle.setText(Body);
        rightbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                if (Remember.isChecked()) {
                    Background.list.add(Key);
                }
                finish();
                // currentContext.startActivity(activityChangeIntent);

            }
        });
    }

    public void ShowCheckbox(String Key) {
        for (String element : Background.list) {
            if (element.equals(Key)) {
                Remember.setChecked(true);
            }
        }

    }




}
