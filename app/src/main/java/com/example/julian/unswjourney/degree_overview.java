package com.example.julian.unswjourney;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar.Tab;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;


public class degree_overview extends AppCompatActivity implements Tab1_degreeoverview.OnFragmentInteractionListener, Tab2_degreeoverview.OnFragmentInteractionListener,Tab3_degreeoverview.OnFragmentInteractionListener{

    private ViewPager viewPager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.degreeoverview_mainpage);




        TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.addTab(tabLayout.newTab().setText("Overview"));
        tabLayout.addTab(tabLayout.newTab().setText("Description"));
        tabLayout.addTab(tabLayout.newTab().setText("Course List"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = findViewById(R.id.pager);
        pagerAdapter_degreeoverview pagerAdapter = new pagerAdapter_degreeoverview(getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });






    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    public void BackOverview(View v) {
        super.onBackPressed();
    }
    public void HelpClickOverview(View v) {
        Intent intent = new Intent(degree_overview.this, Chatbot.class);
        startActivity(intent);
    }

    public void HomeOverview(View v) {
        Intent intent = new Intent(degree_overview.this, MainActivity.class);
        startActivity(intent);
    }
    public void SignOutOverview(View v) {
        FirebaseAuth firebaseAuth;

        //Declaration and defination
        FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null){
                    //Do anything here which needs to be done after signout is complete
                    Intent intent = new Intent(degree_overview.this, Login.class);
                    startActivity(intent);

                }
                else {
                }
            }
        };

//Init and attach
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.addAuthStateListener(authStateListener);

//Call signOut()
        firebaseAuth.signOut();
    }

}
