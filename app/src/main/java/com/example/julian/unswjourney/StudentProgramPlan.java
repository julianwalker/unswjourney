package com.example.julian.unswjourney;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class StudentProgramPlan extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    public static String CurrentPage = "1";
    public static TextView Year;
    TextView Course1;
    TextView Course2;
    TextView Course3;
    TextView Course4;
    TextView Course5;
    TextView Course6;
    TextView Course7;
    TextView Course8;
    TextView Course9;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_planner);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.Cont2);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        Year = (TextView) findViewById(R.id.textViewYear);

        Course1 = findViewById(R.id.c1);
        Course2 = findViewById(R.id.c2);
        Course3 = findViewById(R.id.c3);
        Course4 = findViewById(R.id.c4);
        Course5 = findViewById(R.id.c5);
        Course6 = findViewById(R.id.c6);
        Course7 = findViewById(R.id.c7);
        Course8 =findViewById(R.id.c8);
        Course9 = findViewById(R.id.c9);



// listen for page changes so we can track the current index

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Year.setVisibility(View.VISIBLE);
                Year.setText("Year " + String.valueOf(position + 1));
                CurrentPage = String.valueOf(position + 1);

                Course1.setAlpha(100);

                Course1.setVisibility(View.VISIBLE);
                            Course1.setText(Login.CurrentStudent.ProgramPlan[Integer.valueOf(CurrentPage) - 1 ][0]);
                Course2.setVisibility(View.VISIBLE);
                Course2.setAlpha(100);

                Course2.setText(Login.CurrentStudent.ProgramPlan[Integer.valueOf(CurrentPage) - 1][1]);
                Course3.setVisibility(View.VISIBLE);
                Course3.setAlpha(100);

                            Course3.setText(Login.CurrentStudent.ProgramPlan[Integer.valueOf(CurrentPage) - 1][2]);
                Course4.setVisibility(View.VISIBLE);
                Course4.setAlpha(100);



                Course4.setText(Login.CurrentStudent.ProgramPlan[Integer.valueOf(CurrentPage) - 1][3]);

                Course5.setVisibility(View.VISIBLE);
                Course5.setAlpha(100);


                            Course5.setText(Login.CurrentStudent.ProgramPlan[Integer.valueOf(CurrentPage) - 1][4]);
                Course6.setVisibility(View.VISIBLE);
                Course6.setAlpha(100);

                            Course6.setText(Login.CurrentStudent.ProgramPlan[Integer.valueOf(CurrentPage) - 1][5]);
                Course7.setVisibility(View.VISIBLE);
                Course7.setAlpha(100);

                            Course7.setText(Login.CurrentStudent.ProgramPlan[Integer.valueOf(CurrentPage) - 1][6]);
                Course8.setVisibility(View.VISIBLE);
                Course8.setAlpha(100);
                Course8.setText(Login.CurrentStudent.ProgramPlan[Integer.valueOf(CurrentPage) - 1][7]);

                Course9.setVisibility(View.VISIBLE);
                Course9.setAlpha(100);
                            Course9.setText(Login.CurrentStudent.ProgramPlan[Integer.valueOf(CurrentPage) - 1][8]);


            }

            @Override
            public void onPageSelected(int position) {
                System.out.println("Selected");
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                System.out.println("StateChanged");

            }
        });


    }

    public void BackPlan(View v) {
        super.onBackPressed();
    }
    public void HelpClickPlan(View v) {
        Intent intent = new Intent(StudentProgramPlan.this, Chatbot.class);
        startActivity(intent);
    }
    public void HomePlan(View v) {
        Intent intent = new Intent(StudentProgramPlan.this, MainActivity.class);
        startActivity(intent);
    }
    public void SignOutPlan(View v) {
        FirebaseAuth firebaseAuth;

        //Declaration and defination
        FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null){
                    //Do anything here which needs to be done after signout is complete
                    Intent intent = new Intent(StudentProgramPlan.this, Login.class);
                    startActivity(intent);

                }
                else {
                }
            }
        };

//Init and attach
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.addAuthStateListener(authStateListener);

//Call signOut()
        firebaseAuth.signOut();
    }



    public void SaveClick(View v) {
        ProgramPlan programplan = new ProgramPlan();
        programplan.SaveProgram();
        Toast.makeText(this, "Program Saved", Toast.LENGTH_LONG).show();

    }

    public void DefaultLoad(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Confirm");
        builder.setMessage("Are you sure you want to load the default program?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                Login.CurrentStudent.setProgramPlan(Login.CurrentStudent.ConvertProgramPLanToArray("[[ACCT1501, INFS1602, INFS1609, INFS2621, INFS1603, MATH1041, INFS2603, INFS2605, Open], [GENS4015, INFS2608, INFS2631, INFS3603, ECON1101, MGMT1001, INFS3634, INFS3604, Open], [INFS3617, INFS3830, ECON1102, INFS3605, MGMT1101, MGMT3721, FINS1613, PHYS1160, Open]]"));
                ProgramPlan programplan = new ProgramPlan();
                programplan.SaveProgram();
                //         startActivity(new Intent(StudentProgramPlan.this, StudentProgramPlan.class));
                dialog.dismiss();
                finish();
                startActivity(getIntent());
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }


    public  void probabony(String Position, String Course){

        switch (Position) {
            case "1":
                Course1.setText(Course);
                break;
            case "2":
                Course2.setText(Course);
                break;
            case "3":
                Course3.setText(Course);
                break;
            case "4":
                Course4.setText(Course);
                break;
            case "5":
                Course5.setText(Course);;
                break;
            case "6":
                Course6.setText(Course);
                break;
            case "7":
                Course7.setText(Course);
                break;
            case "8":
                Course8.setText(Course);
                break;
            case "9":
                Course9.setText(Course);
                break;

        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_student_program_plan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    public static class PlaceholderFragment extends Fragment implements View.OnTouchListener {

        private static final String ARG_SECTION_NUMBER = "1";
        public static final int DATEPICKER_FRAGMENT = 1;

        public PlaceholderFragment() {

        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);

            fragment.setArguments(args);
            return fragment;
        }

        @SuppressLint("ResourceType")
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


            inflater = getActivity().getLayoutInflater();
            View rootView = inflater.inflate(R.layout.activity_program_planner, container, false);

            TextView Year2 = (TextView) rootView.findViewById(R.id.textViewYear);
            Year2.setVisibility(rootView.INVISIBLE);

            TextView Course1 = (TextView)rootView.findViewById(R.id.c1);
            Course1.setId(1);
            Course1.setOnTouchListener(this);

            TextView Course2 = (TextView) rootView.findViewById(R.id.c2);
            Course2.setId(2);
            Course2.setOnTouchListener(this);

            TextView Course3 = (TextView) rootView.findViewById(R.id.c3);

            Course3.setId(3);
            Course3.setOnTouchListener(this);

            TextView Course4 = (TextView) rootView.findViewById(R.id.c4);

            Course4.setId(4);
            Course4.setOnTouchListener(this);

            TextView Course5 = (TextView) rootView.findViewById(R.id.c5);

            Course5.setId(5);
            Course5.setOnTouchListener(this);

            TextView Course6 = (TextView) rootView.findViewById(R.id.c6);

            Course6.setId(6);
            Course6.setOnTouchListener(this);

            TextView Course7 = (TextView) rootView.findViewById(R.id.c7);

            Course7.setId(7);
            Course7.setOnTouchListener(this);

            TextView Course8 = (TextView) rootView.findViewById(R.id.c8);

            Course8.setId(8);
            Course8.setOnTouchListener(this);

            TextView Course9 = (TextView) rootView.findViewById(R.id.c9);
            Course9.setId(9);
            System.out.println("DEBUG CURRENT PAGE"+ CurrentPage);
            Course9.setOnTouchListener(this);



            return rootView;


        }


        @Override
        public boolean onTouch(View v, MotionEvent event) {
            // check which textview it is and do what you need to do


            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                System.out.println("Textview " + String.valueOf(v.getId()));
                FragmentManager fm = getActivity().getSupportFragmentManager();
                PlanPickerDialog dialog = new PlanPickerDialog();

                Bundle info = new Bundle();
                info.putString("text_pos", String.valueOf(v.getId()));
                info.putString("year_pos", CurrentPage);


                if (dialog == null || (!dialog.isVisible())) {
                    dialog.setTargetFragment(PlaceholderFragment.this, DATEPICKER_FRAGMENT);
                    dialog.setArguments(info);
                    dialog.show(fm, "Custom Dialog Test");
                }


                // return true if you don't want it handled by any other touch/click events after this
            }
            return true;

        }

        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            // Make sure fragment codes match up
            if (requestCode == PlaceholderFragment.DATEPICKER_FRAGMENT) {

                boolean TempAnswer = Boolean.valueOf(data.getStringExtra("Answer"));
                if (TempAnswer) {
                    ((StudentProgramPlan)getActivity()).probabony(data.getStringExtra("Position"),  data.getStringExtra("Course"));
                    String[][] TempPlan = Login.CurrentStudent.getProgramPlan();
                    int tempposition = Integer.valueOf(data.getStringExtra("Position"));
                    int tempyear = Integer.valueOf(data.getStringExtra("Year"));
                    TempPlan[tempyear - 1][tempposition - 1] = data.getStringExtra("Course");
                    System.out.println(Login.CurrentStudent.ConvertPlanToString(TempPlan));
                } else {
                    Toast.makeText(getActivity(), "Sorry, You do not meet the perquisites.", Toast.LENGTH_LONG).show();
                }
            }


        }
    }



    // For the Sections. No need to touch

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            System.out.println("FM");
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);

        }

        @Override
        public int getCount() {
            // Show 3 total pages.

            return 3;
        }


    }





}


