package com.example.julian.unswjourney;

public class MapLocation {
    private String location;
    private Double lat;
    private Double longitude;

    public MapLocation(){

    }

    public MapLocation(String location, Double lat, Double longitude) {
        this.location = location;
        this.lat = lat;
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return location;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
