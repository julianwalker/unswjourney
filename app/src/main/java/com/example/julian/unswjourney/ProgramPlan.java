package com.example.julian.unswjourney;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Stack;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProgramPlan {
    String operators_match =  "\\(|\\)|AND|OR";
    String course_match = "[A-Z]{4}[0-9]{4}";
    String full_match = operators_match+'|'+course_match;
    JSoupScraper yes = new JSoupScraper();


    // Checks

    // Course Prereq Check
    public  Boolean PreReqGrab(String CourseCode, int year, int position) {
        String jsonObjectAsString = null;

        if (CourseCode.equals("Open")) {
            return true;
        } else {
            try {
                for (int i = 0; i < yes.Students3PlusArray.length(); i++) {
                    JSONObject jsonObject = yes.Students3PlusArray.getJSONObject(i);
                    String TestData = null;
                    TestData = jsonObject.getString("code");
                    if (TestData.equals(CourseCode)) {
                        jsonObjectAsString = jsonObject.getString("body");
                        System.out.println("PREREQ " + jsonObjectAsString);


                    }
                }
            } catch(JSONException e){
                e.printStackTrace();
            }
            if (!jsonObjectAsString.isEmpty()) {
                return Shunting_Yard(CourseGrab(year, position), jsonObjectAsString);
            } else {
                return true;
            }
        }


    }



    public ArrayList<String> CourseGrab(int year, int position) {
        final ArrayList<String> Clashes = new ArrayList<String>();

        int check = 9;
        for(int i=0; i<=year - 1; i++) {
            if (i == year - 1) {
                check = position - 1;
            }
            for(int j=0; j<check; j++) {
                if (!Login.CurrentStudent.getProgramPlan()[i][j].equals("Open")){
                    System.out.println("DEBUD CLASS ADDED " + Login.CurrentStudent.getProgramPlan()[i][j]);

                    Clashes.add(Login.CurrentStudent.getProgramPlan()[i][j]);
                }
                };
            }
            return Clashes;
        }



    public boolean TermChcck(String Course, int position) {
      //  System.out.println("Term Check" + String.valueOf(position) + "  " + Course);
        int jsonObjectAsString = 1;
        try {
            for (int i = 0; i < yes.Students3PlusArray.length(); i++) {
                JSONObject jsonObject = yes.Students3PlusArray.getJSONObject(i);
                String TestData = jsonObject.getString("code");
                if (TestData.equals(Course)) {
                   if (position <= 3 ) {
                       jsonObjectAsString = jsonObject.getInt("t1");
                       System.out.println("Term Check Number 3" + String.valueOf(jsonObjectAsString));

                   } else if (position >=4 && position <= 6 ) {
                       jsonObjectAsString = jsonObject.getInt("t2");
                       System.out.println("Term Check Number 6" + String.valueOf(jsonObjectAsString));

                   } else if (position >=7 && position <= 9 ) {
                       jsonObjectAsString = jsonObject.getInt("t3");
                       System.out.println("Term Check Number 9" + String.valueOf(jsonObjectAsString));

                   }
                }



            }

            if (jsonObjectAsString == 1) {
                return true;
            } else {
                return false;
            }

        } catch(JSONException e){
            e.printStackTrace();
        }
        return true;
    }


    // Program Enrollment Check
    private boolean ProgramCheck() {
        return true;

    }


    // Loads

    // Save Program Data to database and array currently in the program
    public void SaveProgram() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + Login.CurrentStudent.getUniqueID());
        mdatabaseReference.child("ProgramPlan").setValue(Login.CurrentStudent.ConvertPlanToString(Login.CurrentStudent.getProgramPlan()));
        System.out.println("Save Plan " + Login.CurrentStudent.ConvertPlanToString(Login.CurrentStudent.getProgramPlan()));


    }



// Don't even bother trying to understand this

public  Boolean Shunting_Yard(ArrayList<String> CoursesDone, String TestString) {


    String test_string = TestString;
  //  String[] CoursesDone = new String[]{"INFS1602", "ACCT1501"};
    Stack<String> operators = new Stack<String>();
    Stack<Boolean> courses = new Stack<Boolean>();

    //  ArrayList<String> operators = new ArrayList<String>();
    Pattern p = Pattern.compile(full_match);
    Pattern r = Pattern.compile(course_match);
    Pattern s = Pattern.compile(operators_match);

    Matcher matcher = r.matcher(test_string);
try {
    if (!matcher.find()) {
        return true;
    }

    for (Matcher m = p.matcher(test_string); m.find(); ) {
        Matcher n = r.matcher(m.group());
        if (n.find()) {
            System.out.println(" SHUNT TEST " + m.group());
            courses.add(CoursesDone.contains(m.group()));
            //   courses.add(Arrays.asList(CoursesDone).contains(m.group()));

        } else if (s.matcher(m.group()).find()) {
            if (m.group().equals(")")) {
                String operator = operators.pop();
                while (!operator.equals("(")) {
                    Boolean Crs1 = courses.pop();
                    Boolean Crs2 = courses.pop();

                    if (operator.equals("AND")) {
                        Boolean result = Crs1 && Crs2;
                        courses.add(result);
                        operator = operators.pop();
                    } else if (operator.equals("OR")) {
                        Boolean result = Crs1 || Crs2;
                        courses.add(result);
                        operator = operators.pop();
                    }


                }
            } else {
                operators.add(m.group());

            }

        }

    }

    Iterator<String> iter = operators.iterator();

    while (!operators.isEmpty()) {
        String next = operators.pop();
        Boolean crs1 = courses.pop();
        Boolean crs2 = courses.pop();
        if (next.equals("AND")) {
            Boolean result = crs1 && crs2;
            courses.add(result);
        } else if (next.equals("OR")) {
            Boolean result = crs1 || crs2;
            courses.add(result);
        }

    }
    return courses.pop();
} catch (Exception e) {
    System.out.println(e);
    return true;
}
}

    // if we ever have the user enter the number of years this is how we'd do it
    private void CreatePlanArray(int num1, int num2) {
        String[][] multi = new String[num1][num2];
    }






}

