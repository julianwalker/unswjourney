package com.example.julian.unswjourney;

import java.util.List;

public class Notification {


    String Title;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getBody() {
        return Body;
    }

    public void setBody(String body) {
        Body = body;
    }

    String Body;

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    String Key;

}
