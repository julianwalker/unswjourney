package com.example.julian.unswjourney;
import android.support.annotation.MainThread;
import android.util.Log;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.widget.Toast.LENGTH_LONG;

public class Login extends AppCompatActivity {
    Student student = new Student();
    PermissionCheck permissioncheck = new PermissionCheck();
    private FirebaseAuth mAuth;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    boolean check = false;
    String zid;
    public static Student CurrentStudent;

    EditText zidTextF;
    EditText passwordTextF;
    TextView warningtext;
    boolean found = false;
    CheckBox rememberMeCB;
     ProgressDialog dialog;
    Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
       // getActionBar().hide();
////
        // app:srcCompat="#000000" 3&4
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
         dialog = new ProgressDialog(Login.this);

        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
        loginBtn = findViewById(R.id.login);
        warningtext = findViewById(R.id.Hello);
        zidTextF = findViewById(R.id.zidTextF);
        passwordTextF = findViewById(R.id.passwordTextF);
        rememberMeCB = findViewById(R.id.rememberMeCB);

        try {
            settings = getApplicationContext().getSharedPreferences("saved login", 0);
            editor = settings.edit();
            String username = settings.getString("zid", String.valueOf(0));
            String password = settings.getString("password", String.valueOf(0));
            if (!username.equals("") && !username.equals("0") && !password.equals("") && !password.equals("0")) {
                zidTextF.setText(username);
                passwordTextF.setText(password);
                rememberMeCB.setChecked(true);
                loginBtn.performClick();
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        System.out.println("Update UI Already Signed in");


    }
    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs
    }

    String zidToZmail(String zid){
        return  zid + "@student.unsw.edu.au";
    }


    public void LoginClicked(View view) {
       if (permissioncheck.isInternetAvailable(this)) {
            dialog.setMessage("Signing in..Please Wait. ");
            dialog.setCancelable(false);
            dialog.show();
            mAuth.signInWithEmailAndPassword(zidToZmail(zidTextF.getText().toString()), passwordTextF.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {


                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                zid = zidTextF.getText().toString();
                                // Sign in success, update UI with the signed-in user's information
                                System.out.println("signInWithEmail:success");
                                //FirebaseUser user = mAuth.getCurrentUser();
                                if (rememberMeCB.isChecked()) {
                                    editor.putString("zid", zidTextF.getText().toString());
                                    editor.putString("password", passwordTextF.getText().toString());
                                } else {
                                    editor.putString("zid", "");
                                    editor.putString("password", "");
                                }
                                System.out.println("Login: True");


                                FirebaseUser currentUser = mAuth.getCurrentUser();
                                System.out.println(currentUser.getUid());
                                CheckUser(currentUser.getUid());

                                editor.apply();
                                //zidTextF.setText("");
                                passwordTextF.setText("");


                                //rememberMeCB.setChecked(false);


                           } else {
                                // If sign in fails, display a message to the user.
                             Toast.makeText(Login.this, "Login Failure", LENGTH_LONG).show();
                              System.out.println("Login: Failture");
                                dialog.dismiss();
                                // Log.w(TAG, "createUserWithEmail:failure", task.getException());
                           }
                      }

                    });
        } else {
       //     Toast.makeText(Login.this, "No Internet Detected: Login disabled.", LENGTH_LONG).show();
       //     System.out.println("Login: Failture");
      //      dialog.dismiss();
      }



    }

    public void CheckUser(final String USERID) {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();
        MaxDatabase.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                    if (answerSnapshot.getKey().equals(USERID)) {
                        System.out.println("User Found");
                        student.LoadData(USERID);
                        check = true;
                    }
                }
                if (check == false) {
                    System.out.println("User not found");
                    CreateUserDatabase(USERID);
                }
                OnboardCheck();
                dialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void CreateUserDatabase(String USERID) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mdatabaseReference = database.getReference("Users/" + USERID);
        mdatabaseReference.child("Name").setValue("TEST");


    }

    public void  Getinfo() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Name, email address, and profile photo Url
            String name = user.getDisplayName();
            String email = user.getEmail();
            Uri photoUrl = user.getPhotoUrl();

            // Check if user's email is verified
            boolean emailVerified = user.isEmailVerified();

            // The user's ID, unique to the Firebase project. Do NOT use this value to
            // authenticate with your backend server, if you have one. Use
            // FirebaseUser.getToken() instead.
            String uid = user.getUid();
        }
    }


    private void OnboardCheck() {

        SharedPreferences settings = this.getSharedPreferences("Onboarding", 0);
        boolean firstTime = settings.getBoolean("first_time", true);

        if (firstTime) {
            startActivity(new Intent(Login.this, OnboardingFirstPage.class));

        } else {
            startActivity(new Intent(Login.this, MainActivity.class));

        }

    }

    public void FORGOTPW(View v) {
        String url = "https://www.it.unsw.edu.au/students/unipass/#IhavelostorforgottenmyUniPass";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
