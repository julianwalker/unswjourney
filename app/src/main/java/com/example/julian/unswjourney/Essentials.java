package com.example.julian.unswjourney;


// Store useful repeating functions here


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

public class Essentials {

    // Make Toast
    public void MakeToast(String i, Context context) {
        Toast.makeText(context, i, Toast.LENGTH_LONG).show();
    }

// print line
public void PrintLine(String i) {
    System.out.println(i);
}



    // Alert Dialog


    public void SimpleAlert(String Title, String Body, Context context) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(Title);
        alertDialog.setMessage(Body);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


}
