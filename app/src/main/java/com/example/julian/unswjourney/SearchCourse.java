package com.example.julian.unswjourney;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchCourse extends AppCompatActivity {

    EditText ProgramSearchBox;
    ListView ProgramSearchList;
    ArrayAdapter<String> adapter1;
    String[] PlusJsonToArray;
    JSoupScraper mJSoupScraper = new JSoupScraper();
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        getActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_course);

        dialog = new ProgressDialog(this);


        ProgramSearchBox =(EditText) findViewById(R.id.SearchBar);
        ProgramSearchList =(ListView) findViewById(R.id.SearchList);
        ProgramSearchList.setTextFilterEnabled(true);

        PlusJsonToArray =  ListViewPopulate();
        adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, PlusJsonToArray);
        ProgramSearchList.setAdapter(adapter1);


        ProgramSearchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(SearchCourse.this, CourseViewer.class);
                intent.putExtra("Name", ProgramSearchList.getItemAtPosition(i).toString());
                startActivity(intent);
            }
        });

        // Set the TextWatcher aka real time legit search 2.0
        ProgramSearchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                SearchCourse.this.adapter1.getFilter().filter(charSequence);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    public String[] ListViewPopulate() {
        String[] thisIsAStringArray = new String[mJSoupScraper.Students3PlusArray.length()];
        dialog.setMessage("Doing something, please wait.");
        dialog.show();
        try {
            final ArrayList<String> TempListAdder = new ArrayList<String>();
            for (int i = 0; i < mJSoupScraper.Students3PlusArray.length(); i++) {
                JSONObject jsonObject = null;
                jsonObject = mJSoupScraper.Students3PlusArray.getJSONObject(i);
                String TestData = null;
                TestData = jsonObject.getString("code");
                TempListAdder.add(TestData);
                thisIsAStringArray[i] = TestData;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.dismiss();
        return thisIsAStringArray;

    }





    public void BackSearch(View v) {
        super.onBackPressed();
    }
    public void HelpClickSearch(View v) {
        Intent intent = new Intent(SearchCourse.this, Chatbot.class);
        startActivity(intent);
    }
    public void HomeSearch(View v) {
        Intent intent = new Intent(SearchCourse.this, MainActivity.class);
        startActivity(intent);
    }
    public void SignOutSearch(View v) {
        FirebaseAuth firebaseAuth;

        //Declaration and defination
        FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null){
                    //Do anything here which needs to be done after signout is complete
                    Intent intent = new Intent(SearchCourse.this, Login.class);
                    startActivity(intent);

                }
                else {
                }
            }
        };

//Init and attach
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.addAuthStateListener(authStateListener);

//Call signOut()
        firebaseAuth.signOut();
    }


}
