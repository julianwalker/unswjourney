package com.example.julian.unswjourney;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;


import java.util.ArrayList;
import java.util.List;

import ai.api.AIDataService;
import ai.api.AIListener;
import ai.api.AIServiceException;
import ai.api.android.AIConfiguration;
import ai.api.android.AIService;
import ai.api.model.AIError;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;

public class Chatbot extends Activity implements AIListener {
    RecyclerView recyclerView;
    EditText editText;
    RelativeLayout addBtn;
    Boolean flagFab = true;
    private AIService aiService;
     List<ChatMessage> MessageList ;
     ChatAdapter chatAdapter ;

    private AIDataService aiDataService;
    private AIResponse test;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_chatbot);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        editText = (EditText)findViewById(R.id.editText);

        addBtn = (RelativeLayout)findViewById(R.id.addBtn);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);



        final ai.api.android.AIConfiguration config = new
                ai.api.android.AIConfiguration("8693db38589d4c4fa35cf440e7d4a597",
                ai.api.android.AIConfiguration.SupportedLanguages.English,
                ai.api.android.AIConfiguration.RecognitionEngine.System);

        // Use with text search
        aiDataService = new ai.api.android.AIDataService(this, config);
        // Use with Voice input
        aiService = AIService.getService(this, config);
        aiService.setListener(this);

       // Send a First Message
        MessageList =  new ArrayList<ChatMessage>();
        send("Hello");
        //ChatMessage msgDto = new ChatMessage("Hello", "user");
        //MessageList.add(msgDto);
        chatAdapter = new ChatAdapter(MessageList);

        // Set Data Adapter
        // Create it
        recyclerView.setAdapter(chatAdapter);

    }

    public void AddButton(View view) {

        String message = editText.getText().toString().trim();

        if (!message.equals("")) {

            // So it isn't empty. lets add it to the adapter before sending
            ChatMessage Listmessage = new ChatMessage(message, "user");
            MessageList.add(Listmessage);
            int newmessageposition = MessageList.size() - 1;
            // Instert into recycle view
            chatAdapter.notifyItemInserted(newmessageposition);
            // Scroll it to the message
            recyclerView.scrollToPosition(newmessageposition);
            send(message);

        }

        editText.setText("");

    }

    public void send(String n) {
        AIRequest aiRequest = new AIRequest();

        aiRequest.setQuery(n);

        if(aiRequest==null) {
            throw new IllegalArgumentException("aiRequest must be not null");
        }

        final AsyncTask<AIRequest, Integer, AIResponse> task =
                new AsyncTask<AIRequest, Integer, AIResponse>() {
                    private AIError aiError;

                    @Override
                    protected AIResponse doInBackground(final AIRequest... params) {
                        final AIRequest request = params[0];
                        try {
                            final AIResponse response =    aiDataService.request(request);
                            // Return response
                            test = response;
                            return response;
                        } catch (final AIServiceException e) {
                            aiError = new AIError(e);
                            return null;
                        }
                    }

                    @Override
                    protected void onPostExecute(final AIResponse response) {
                        if (response != null) {
                            onResult(response);
                        } else {
                            onError(aiError);
                        }
                    }
                };
        task.execute(aiRequest);
    }

    @Override
    public void onResult(AIResponse result) {
        Result yes = test.getResult();
        String speech = yes.getFulfillment().getSpeech();
        System.out.println(speech + " :Was the speech");
        // So it isn't empty. lets add it to the adapter before sending
        ChatMessage Listmessage = new ChatMessage(speech, "bot");
        MessageList.add(Listmessage);
        int newmessageposition = MessageList.size() - 1;
        // Instert into recycle view
        chatAdapter.notifyItemInserted(newmessageposition);
        // Scroll it to the message
        recyclerView.scrollToPosition(newmessageposition);


    }

    @Override
    public void onError(AIError error) {

    }

    @Override
    public void onAudioLevel(float level) {

    }

    @Override
    public void onListeningStarted() {

    }

    @Override
    public void onListeningCanceled() {

    }

    @Override
    public void onListeningFinished() {

    }
    }

