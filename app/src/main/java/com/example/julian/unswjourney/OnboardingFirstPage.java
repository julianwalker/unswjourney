package com.example.julian.unswjourney;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class OnboardingFirstPage extends AppCompatActivity{

    private Button nextButton;
    private Button skipButton;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.onboarding_intro_menu);

        SharedPreferences settings = this.getSharedPreferences("Onboarding", 0);
        boolean firstTime = settings.getBoolean("first_time", true);

        if (firstTime) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("first_time", false);
            editor.commit();
        }

        nextButton = (Button) findViewById(R.id.buttonContinue);
        skipButton=(Button) findViewById(R.id.buttonSkip);

        TextView txt = (TextView) findViewById(R.id.alertybody);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/optima.ttf");
        txt.setTypeface(font);


        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("reading");
                try {
                    Intent intent = new Intent(getApplicationContext(), OnboardingSlide.class);
                    System.out.println("Done intent check");

                    startActivity(intent);
                }catch (Exception e){
                    System.out.println(e);
                }


            }
        });


    }

    public void SkipClick(View v) {
        Intent intent = new Intent(OnboardingFirstPage.this, MainActivity.class);
        startActivity(intent);

    }




}
