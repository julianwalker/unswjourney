package com.example.julian.unswjourney;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PlanPickerDialog extends DialogFragment{
    TextView TestTest;
    Button Testbutton;
    Button SetFillButton;

    TextView SelectedText;

String text_info;
    String year_info ;

EditText ProgramSearchBox;
ListView ProgramSearchList;
    JSoupScraper mJSoupScraper = new JSoupScraper();
    ArrayAdapter<String> adapter1;
    ProgressDialog dialog;
    String[] PlusJsonToArray;
    ProgramPlan programplan = new ProgramPlan();
    Essentials essentials = new Essentials();

    Boolean Answer;
    String TempCourse;
    String TempPosition;
    String TempYear;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Answer = null;
        View view = inflater.inflate(R.layout.activity_program_plan_dialog, container, false);
       // TestTest = (TextView) view.findViewById(R.id.TestString);
        SelectedText = (TextView) view.findViewById(R.id.SelectedText);

        Testbutton = (Button) view.findViewById(R.id.testButton);
        SetFillButton = (Button) view.findViewById(R.id.FillSubject);

        SetFillButton.setOnClickListener(new View.OnClickListener() {
                                             public void onClick(View v) {
                                                 // Perform action on click
                                                 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                                                 builder.setTitle("Confirm");
                                                 builder.setMessage("Are you sure you want to fill this subject?");

                                                 builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                                                     public void onClick(DialogInterface dialog, int which) {
                                                         SubmitCourse((String) "Open", text_info, year_info, true);
                                                         dialog.dismiss();
                                                     }
                                                 });

                                                 builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                                                     @Override
                                                     public void onClick(DialogInterface dialog, int which) {

                                                         dialog.dismiss();
                                                     }
                                                 });

                                                 AlertDialog alert = builder.create();
                                                 alert.show();

                                             }
                                         });






        Testbutton.setOnClickListener(v -> SubmitCourse((String) SelectedText.getText(),text_info,year_info, false ));


        ProgramSearchBox =(EditText) view.findViewById(R.id.ProgramSearchBox);
        ProgramSearchList =(ListView)view.findViewById(R.id.ProgramSearchList);
        ProgramSearchList.setTextFilterEnabled(true);
        dialog = new ProgressDialog(getActivity());


        // Set the Adapter
        PlusJsonToArray =  ListViewPopulate();
        adapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, PlusJsonToArray);
        ProgramSearchList.setAdapter(adapter1);

        ProgramSearchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SelectedText.setText((String) ProgramSearchList.getItemAtPosition(i));
            }
        });

        // Set the TextWatcher aka real time legit search 2.0
        ProgramSearchBox.addTextChangedListener(new TextWatcher() {
                                                     @Override
                                                     public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                         PlanPickerDialog.this.adapter1.getFilter().filter(charSequence);
                                                     }

                                                     @Override
                                                     public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                                     }

                                                     @Override
                                                     public void afterTextChanged(Editable editable) {

                                                     }
                                                 });


        Bundle info = getArguments();
        text_info = info.getString("text_pos");
        year_info = info.getString("year_pos");
        System.out.println("GOT STRING" + text_info + "GOT YEAR " + year_info);

//        TestTest.setText(text_info);


        return view;
    }

    public String[] ListViewPopulate() {
        String[] thisIsAStringArray = new String[mJSoupScraper.Students3PlusArray.length()];
        dialog.setMessage("Doing something, please wait.");
        dialog.show();
        try {
            final ArrayList<String> TempListAdder = new ArrayList<String>();
            for (int i = 0; i < mJSoupScraper.Students3PlusArray.length(); i++) {
                JSONObject jsonObject = null;
                jsonObject = mJSoupScraper.Students3PlusArray.getJSONObject(i);
                String TestData = null;
                TestData = jsonObject.getString("code");
                TempListAdder.add(TestData);
                thisIsAStringArray[i] = TestData;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dialog.dismiss();
        return thisIsAStringArray;

    }


    public void SubmitCourse(String Course, String Position, String Year, Boolean Open) {
    TempCourse = Course;
    TempPosition = Position;
    TempYear = Year;


    if (Open) {
        Answer = true;
        sendResult(1);
    } else {
        if (!Course.isEmpty()) {
            System.out.println("Submit " + Year + " " + Position + " " + Course);
            System.out.println("Term check Output" + programplan.TermChcck(Course, Integer.valueOf(Position)));
            Answer = programplan.TermChcck(Course, Integer.valueOf(Position)) && programplan.PreReqGrab(Course, Integer.valueOf(Year), Integer.valueOf(Position));
            System.out.println("Output: " + String.valueOf(Answer));
            sendResult(1);
        } else {
            Toast.makeText(getActivity(), "Sorry, Please Select a Course", Toast.LENGTH_LONG).show();

        }
    }





    }


    // Send my stuff back boi
    private void sendResult(int REQUEST_CODE) {
        Intent intent = new Intent();

        intent.putExtra("Answer", String.valueOf(Answer));
        intent.putExtra("Course", TempCourse);
        intent.putExtra("Year", TempYear);
        intent.putExtra("Position", TempPosition);
        getTargetFragment().onActivityResult(
                getTargetRequestCode(), REQUEST_CODE, intent);
this.dismiss();
    }





}



