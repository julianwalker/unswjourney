package com.example.julian.unswjourney;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Background extends Service {
    private Thread repeatTaskThread;
    public static List<Notification> NotificationList = new ArrayList<Notification>();
    String temptitle;
    String Tempbody;
    String Tempkey;
    public static  List<String> list =   new ArrayList<String>();



    Handler handler = new Handler();
    Runnable turnBlack = new Runnable() {
        @Override
        public void run() {
            handler.postDelayed(turnBlack, 500000);
            FirebaseDatabase data = FirebaseDatabase.getInstance();
            DatabaseReference MaxDatabase = data.getReference();
            AddtoList();

            MaxDatabase.child("Notifications").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                        Notification temp = new Notification();
                        Tempkey = String.valueOf(answerSnapshot.getKey());
                        for (DataSnapshot InnerSnapshop : answerSnapshot.getChildren()) {
                            if (InnerSnapshop.getKey().equals("Title")) {

                                temptitle = InnerSnapshop.getValue(String.class);
                                System.out.println(temptitle);
                                temp.setKey(Tempkey);
                                temp.setTitle(InnerSnapshop.getValue(String.class));
                            }
                            if (InnerSnapshop.getKey().equals("Body")) {
                                Tempbody = InnerSnapshop.getValue(String.class);

                                temp.setBody(InnerSnapshop.getValue(String.class));

                            }


                        }
                        if (checkForValue(temptitle)) {
                            // Need to set course if in list.

                            if (!list.contains(Tempkey)) {
                                System.out.println("ADDED TO NOTO LIST");

                                SendNotification(temptitle, Tempbody, Tempkey);
                            }
                            NotificationList.add(temp);

                        }

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
            SaveToList();
        }

    };

    private boolean checkForValue(String val){
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 8; j++){
                if(Login.CurrentStudent.getProgramPlan()[i][j].equals(val)) return true;
            }
        }
        return false;
    }


    public  void AddtoList() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String serialized = sp.getString("Alist", null);
        if (serialized != null) {
            list = Arrays.asList(TextUtils.split(serialized, ","));

        }

    }

    public void SaveToList() {
        if (!list.isEmpty()) {
            SharedPreferences settings = getSharedPreferences("Alist", MODE_PRIVATE);
            SharedPreferences.Editor prefEditor = settings.edit();
            prefEditor.putString("Alist", TextUtils.join(",", list));
        }

    }

    @Override
    public void onCreate() {
        System.out.println("Service Started");

    }

    /** The service is starting, due to a call to startService() */
    @Override
    // Let it continue running until it is stopped.
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler.postDelayed(turnBlack, 5000);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /** Called when The service is no longer used and is being destroyed */
    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("Service Stopped");


    }


    public void SendNotification(String a, String b, String c) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {


                Intent notificationIntent = new Intent(Background.this, AlertDialog.class);
                notificationIntent.putExtra("Title", a);
                notificationIntent.putExtra("Body", b);
                notificationIntent.putExtra("Key", c);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(Background.this, "1")
                        .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark) //icon
                        .setContentTitle(a)
                        .setContentText("A Course Has Changed. Click to view!")
                        .setContentIntent(PendingIntent.getActivity(Background.this, 0, notificationIntent, PendingIntent.FLAG_ONE_SHOT))
                        .setAutoCancel(true)//swipe for delete
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(1, mBuilder.build());            }

        };
        Thread thread = new Thread(runnable);
        thread.start();
    }





}

