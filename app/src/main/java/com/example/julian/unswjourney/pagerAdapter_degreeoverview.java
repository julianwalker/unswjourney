package com.example.julian.unswjourney;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class pagerAdapter_degreeoverview extends FragmentStatePagerAdapter {

    int mNoOfTabs;


    public pagerAdapter_degreeoverview(FragmentManager fm, int NumberOfTabs) {
        super(fm);
        this.mNoOfTabs = NumberOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new Tab1_degreeoverview();
            case 1: return new Tab2_degreeoverview();
            case 2: return new Tab3_degreeoverview();
        }
        return null;
    }
    @Override
    public int getCount() {
        return 3;
    }



}


