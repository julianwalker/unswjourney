package com.example.julian.unswjourney;

import android.os.Build;
import android.support.annotation.RequiresApi;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;

public class Student {

    String zid;
    String studentName;
    String zmail;

    public String getUniqueID() {
        return UniqueID;
    }

    public void setUniqueID(String uniqueID) {
        UniqueID = uniqueID;
    }

    String UniqueID;
    String[][] ProgramPlan = new String[3][8];

    public String getZid() {
        return zid;
    }

    public void setZid(String zid) {
        this.zid = zid;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getZmail() {
        return zmail;
    }

    public void setZmail(String zmail) {
        this.zmail = zmail;
    }

    public String[][] getProgramPlan() {
        return ProgramPlan;
    }

    public void setProgramPlan(String[][] programPlan) {
        ProgramPlan = programPlan;
    }


// program plan array



public void LoadData(String UniqueID) {

    FirebaseDatabase data = FirebaseDatabase.getInstance();
    DatabaseReference MaxDatabase = data.getReference();
    Student tempstudent = new Student();

    tempstudent.setUniqueID(UniqueID);
    MaxDatabase.child("Users").child(UniqueID).addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {


                    if (answerSnapshot.getKey().equals("ProgramPlan")) {
                        tempstudent.setProgramPlan(ConvertProgramPLanToArray(answerSnapshot.getValue(String.class)));
                        System.out.println(answerSnapshot.getValue(String.class));
                    }
                    if (answerSnapshot.getKey().equals("zmail")) {
                        tempstudent.setZmail(answerSnapshot.getValue(String.class));
                        System.out.println(answerSnapshot.getValue(String.class));

                    }
                    if (answerSnapshot.getKey().equals("zid")) {
                        tempstudent.setZid(answerSnapshot.getValue(String.class));
                        System.out.println(answerSnapshot.getValue(String.class));

                    }
                    if (answerSnapshot.getKey().equals("Name")) {
                        tempstudent.setStudentName(answerSnapshot.getValue(String.class));
                        System.out.println(answerSnapshot.getValue(String.class));

                }
                }
                Login.CurrentStudent = tempstudent;




        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
        }
    });
}

    public String[][] ConvertProgramPLanToArray(String s){
        String[] rows = s.split("], \\[");
        for (int i = 0; i < rows.length; i++) {
            // Remove any beginning and ending braces and any white spaces
            rows[i] = rows[i].replace("[[", "").replace("]]", "").replaceAll(" ", "");
        }

// Get the number of columns in a row
        int numberOfColumns = rows[0].split(",").length;

// Setup your matrix
        String[][] matrix = new String[rows.length][numberOfColumns];

// Populate your matrix
        for (int i = 0; i < rows.length; i++) {
            matrix[i] = rows[i].split(",");
        }
        return matrix;
    }

    public String ConvertPlanToString(String[][] Plan){
        return Arrays.deepToString(Plan);
    }



}
