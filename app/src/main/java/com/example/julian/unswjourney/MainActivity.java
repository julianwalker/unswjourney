package com.example.julian.unswjourney;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


// Comments

// We will not use main activity. First load will occur in splash (TBA) or Login.
// Test Class Main Screen
public class MainActivity extends AppCompatActivity {
    JSoupScraper yes = new JSoupScraper();
    ProgramPlan programplan = new ProgramPlan();
    Login login = new Login();
    private FirebaseAuth firebaseAuth;
    ProgressDialog dialog;
    Student student = new Student();
    Boolean Check = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
      //  getSupportActionBar().hide();


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(new Intent(getBaseContext(), Background.class));

        dialog = new ProgressDialog(this);
        yes.JSoupScraper(MainActivity.this);
        TextView nametext = findViewById(R.id.welcometext);
        nametext.setText("Welcome, " + Login.CurrentStudent.getStudentName());

        try {
            if (JSoupScraper.Students3PlusArray.isNull(1)) {

            }
        } catch (NullPointerException e) {
            Load3PlusData();
            DStart();

        }


    }

    public void DStart() {
        dialog.setMessage("Loading 3Plus Data...Please Wait");
        dialog.setCancelable(false);
        dialog.show();
    }
    public void DStop() {
    dialog.dismiss();
    }


public void TestButtonClick(View view) throws JSONException {

   /*
    ArrayList<String> Programchecks = programplan.PreReqGrab("ACCT3610", 2, 2);
    if (Programchecks.isEmpty()) {
        System.out.println("CHECKS PASSED");
    } else {
        for (String Clash : Programchecks) {
            System.out.println(Clash);
            Intent intent = new Intent(MainActivity.this, StudentProgramPlan.class);
            startActivity(intent);


        }
    }*/


}

public void SignOutBtnMain(View v) {
    //Declaration and defination
    FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
        @Override
        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
            if (firebaseAuth.getCurrentUser() == null){
                //Do anything here which needs to be done after signout is complete
                Intent intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);

            }
            else {
            }
        }
    };

//Init and attach
    firebaseAuth = FirebaseAuth.getInstance();
    firebaseAuth.addAuthStateListener(authStateListener);

//Call signOut()
    firebaseAuth.signOut();
}

public void CourseViewClick(View v) {
    Intent intent = new Intent(MainActivity.this, SearchCourse.class);
    startActivity(intent);
}
    public void NotiClick(View v) {
        Intent intent = new Intent(MainActivity.this, NotificationManager.class);
        startActivity(intent);
    }

    public void OnbaordClick(View v) {
        Intent intent = new Intent(MainActivity.this, OnboardingFirstPage.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed()
    {
       // super.onBackPressed();  // optional depending on your needs
    }

public void HelpClick(View v){
    Intent intent = new Intent(MainActivity.this, Chatbot.class);
    startActivity(intent);
}

    public void prooverview(View v){
        Intent intent = new Intent(MainActivity.this, degree_overview.class);
        startActivity(intent);
    }
public void DegreePlannerClick(View v) {
    System.out.println(String.valueOf(programplan.PreReqGrab("INFS1602", 1, 2)));
    Intent intent = new Intent(MainActivity.this, StudentProgramPlan.class);
    startActivity(intent);
}

public void MapClick(View v){
    Intent intent = new Intent(MainActivity.this, map_interface.class);
    startActivity(intent);
}


    // Due to shitty threads we should.
    private void TestActivity() throws JSONException {

    }


    // Activate the webview and call the JsoupScrapper!
    private void Load3PlusData() {

       final WebView webview = (WebView) findViewById(R.id.JSoup);

        webview.getSettings().setJavaScriptEnabled(true);

        webview.addJavascriptInterface(new MyJavaScriptInterface(this), "HtmlViewer");

        webview.setWebViewClient(new android.webkit.WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                // webview.loadUrl("javascript:window.HtmlViewer.showHTML" +  "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                webview.loadUrl("javascript:window.HtmlViewer.showHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }
        });

        webview.loadUrl("http://www.student3plus.unsw.edu.au/search/course-list");

    }
    // Scrape 3PLus Data into the array!
     class MyJavaScriptInterface {
        private android.content.Context ctx;

        MyJavaScriptInterface(android.content.Context ctx) {
            this.ctx = ctx;
        }

        @JavascriptInterface
        public void showHTML(String html) throws JSONException {

            Handler refresh = new Handler(Looper.getMainLooper());
            refresh.post(() -> {

                String stripped = Jsoup.parse(html).text();
                System.out.println(stripped);
                try {
                    JSoupScraper.Students3PlusArray = new JSONArray(stripped.toString());
                    DStop();
                } catch (JSONException e) {
                    e.printStackTrace();
                }



            });


        }

    }



}
