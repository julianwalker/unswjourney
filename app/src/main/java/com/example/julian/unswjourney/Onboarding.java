package com.example.julian.unswjourney;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Onboarding extends AppCompatActivity {
    private ViewPager mSlideViewPager;
    private LinearLayout mDotLayout;
    private TextView[] mDots;
    private SliderAdapter sliderAdapter;
    private int mCurrentPage;
    Button mbutton22;

//    @Override
////    protected void onCreate(Bundle savedInstanceState) {
////        super.onCreate(savedInstanceState);
////        setContentView(R.layout.activity_onboarding);
////        mSlideViewPager = (ViewPager) findViewById(R.id.Slidepage);
////        // mDotLayout = (LinearLayout) findViewById(R.id.Dotlayout);
////        mbutton22 = (Button) findViewById(R.id.button22);
////        SharedPreferences settings = this.getSharedPreferences("Onboarding", 0);
////        boolean firstTime = settings.getBoolean("first_time", true);
////
////        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
////        if (firstTime) {
////            SharedPreferences.Editor editor = settings.edit();
////            editor.putBoolean("first_time", false);
////            editor.commit();
////        }
////        sliderAdapter = new SliderAdapter(this);
////        mSlideViewPager.setAdapter(sliderAdapter);
////        mSlideViewPager.addOnPageChangeListener(ViewListiner);

    //}

    @Override
    public void onBackPressed() {
    }
    ViewPager.OnPageChangeListener ViewListiner = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            mCurrentPage = position;

            if (position != 5) {
                mbutton22.setEnabled(false);
                mbutton22.setVisibility(View.INVISIBLE);
            }


            if (position == 5) {
                mbutton22.setEnabled(true);
                mbutton22.setVisibility(View.VISIBLE);
            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };



    public void ContinueClick(View v) {
        startActivity(new Intent(Onboarding.this, MainActivity.class));
    }
    public void AddDotsIndicator() {
        mDots = new TextView[6];

        for (int i=0; i < mDots.length; i++) {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(getResources().getColor(R.color.common_google_signin_btn_text_light));

            mDotLayout.addView(mDots[i]);
        }
    }


}
