package com.example.julian.unswjourney;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class OnboardingSlide extends AppCompatActivity implements View.OnClickListener  {

    private ViewPager mSlideViewPager;
    private SliderAdapter sliderAdapter;
    private LinearLayout Dots_Layout;
    private ImageView[] dots;
    private int[] layouts = {R.layout.onboarding_first_page,R.layout.onboarding_second_page,R.layout.onboarding_third_page, R.layout.onboarding_fourth_page,R.layout.onboarding_fifth_page,R.layout.onboarding_sixth_page,R.layout.onboarding_seventh_page,
    R.layout.onboarding_eight_slide,R.layout.onboarding_ninth_slide};
    private Button nextButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding_slide);


        mSlideViewPager = (ViewPager) findViewById(R.id.slideViewPage);
        sliderAdapter = new SliderAdapter(layouts,this);
        mSlideViewPager.setAdapter(sliderAdapter);
        nextButton=  (Button)findViewById(R.id.buttonNext);
        Dots_Layout = (LinearLayout)findViewById(R.id.dotsLayout);
        createDots(0);
        nextButton.setOnClickListener(this);
        nextButton.setVisibility(View.INVISIBLE);


        mSlideViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                createDots(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });

    }


    private void createDots(int currentPosition){
        if (Dots_Layout != null)
            Dots_Layout.removeAllViews();

        dots = new ImageView[layouts.length];

        for(int i = 0; i< layouts.length;i++){

            if(currentPosition==8){
                nextButton.setVisibility(View.VISIBLE);
                TextView textView1 = findViewById(R.id.textGloss);
                TextView textView2= findViewById(R.id.textViewArtDesign);
                TextView textView3 = findViewById(R.id.textViewArt);
                TextView textView4 = findViewById(R.id.textViewBuiltEnvironment);
                TextView textView5 = findViewById(R.id.textViewBusinessSchool);
                TextView textView6 = findViewById(R.id.textViewEngineer);
                TextView textView7 = findViewById(R.id.textViewLawSchool);
                TextView textView8 = findViewById(R.id.textViewMedicalSchool);
                TextView textView9 = findViewById(R.id.textViewScience);
                textView1.setMovementMethod(LinkMovementMethod.getInstance());
                textView2.setMovementMethod(LinkMovementMethod.getInstance());
                textView3.setMovementMethod(LinkMovementMethod.getInstance());
                textView4.setMovementMethod(LinkMovementMethod.getInstance());
                textView5.setMovementMethod(LinkMovementMethod.getInstance());
                textView6.setMovementMethod(LinkMovementMethod.getInstance());
                textView7.setMovementMethod(LinkMovementMethod.getInstance());
                textView8.setMovementMethod(LinkMovementMethod.getInstance());
                textView9.setMovementMethod(LinkMovementMethod.getInstance());


            }
            else {
                nextButton.setVisibility(View.INVISIBLE);
            }

            if(currentPosition==3){
                TextView txt = findViewById(R.id.textDescCourses);
                txt.setMovementMethod(new ScrollingMovementMethod());

            }



            dots[i] = new ImageView(this);
            if(i == currentPosition){
                dots[i].setImageDrawable(ContextCompat.getDrawable(this,R.drawable.active_dot));
            }
            else {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this,R.drawable.inactive_dot));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            params.setMargins(4,0,4,0 );

            Dots_Layout.addView(dots[i],params);



      }
    }


    @Override
    public void onClick(View view) {
        int id = Integer.valueOf(view.getId());
        if (id == 2131296332) {
            startActivity(new Intent(OnboardingSlide.this, MainActivity.class));

        }
    }
}
