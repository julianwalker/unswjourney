package com.example.julian.unswjourney;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import android.support.v4.app.FragmentActivity;

import java.util.ArrayList;

public class map_interface extends FragmentActivity implements OnMapReadyCallback {
    private static final String TAG = "Yo";


    ListView mListView;
    private String locName;
    private double latitude;
    private double longitude;
    ArrayList<MapLocation> mapLocationArrayList = new ArrayList<>();
    private GoogleMap myMap;
    private EditText searchLocation;
    ArrayAdapter<MapLocation> trialAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_map_interface);
        TextView mapLocation = findViewById(R.id.mapLocationText);
        mListView = findViewById(R.id.listViewLocation);
        searchLocation = findViewById(R.id.editTextSearchLocation);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);

        LoadData();

        searchLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                (map_interface.this).trialAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MapLocation chosenLocation = mapLocationArrayList.get(position);
                double chosenLat = chosenLocation.getLat();
                double chosenLong = chosenLocation.getLongitude();
                String chosenName = chosenLocation.getLocation();
                AddMarker(chosenLat,chosenLong,chosenName);

            }
        });


    }

    public void alterListView(){


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        myMap = googleMap;
        LatLng unsw = new LatLng(-33.919368,151.2308062);
        Marker defaultLocation =  myMap.addMarker(new MarkerOptions().position(unsw).title("UNSW"));
        myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(unsw,16));
        defaultLocation.showInfoWindow();
    }

    private void AddMarker(Double Lat, Double Long, String name){
        LatLng location = new LatLng(Lat, Long);
        System.out.println(Lat + "this is Lat. Long is " + Long);
        Marker hitLocation = myMap.addMarker(new MarkerOptions().position(location).title(name));
        myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location,19));
        hitLocation.showInfoWindow();

    }


    public void LoadData() {
        FirebaseDatabase data = FirebaseDatabase.getInstance();
        DatabaseReference MaxDatabase = data.getReference();
        MaxDatabase.child("Maps").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {
                    for (DataSnapshot InnerSnapshot : answerSnapshot.getChildren()) {
                        locName = answerSnapshot.child("Location").getValue(String.class);
                        latitude = answerSnapshot.child("Lat").getValue(Double.class);
                        longitude = answerSnapshot.child("Long").getValue(Double.class);
                    }
                    MapLocation mapLocation = new MapLocation(locName, latitude, longitude);

                    mapLocationArrayList.add(mapLocation);
                    System.out.println("This is me" + mapLocationArrayList.toString());
                }

                trialAdapter = new ArrayAdapter<MapLocation>
                        (map_interface.this, R.layout.location_for_click, mapLocationArrayList);
                mListView.setAdapter(trialAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void BackMap(View v) {
        super.onBackPressed();
    }
    public void HelpClickMap(View v) {
        Intent intent = new Intent(map_interface.this, Chatbot.class);
        startActivity(intent);
    }
    public void HomeMap(View v) {
        Intent intent = new Intent(map_interface.this, MainActivity.class);
        startActivity(intent);
    }
    public void SignOutBtnMain(View v) {
         FirebaseAuth firebaseAuth;

        //Declaration and defination
        FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null){
                    //Do anything here which needs to be done after signout is complete
                    Intent intent = new Intent(map_interface.this, Login.class);
                    startActivity(intent);

                }
                else {
                }
            }
        };

//Init and attach
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.addAuthStateListener(authStateListener);

//Call signOut()
        firebaseAuth.signOut();
    }




}
