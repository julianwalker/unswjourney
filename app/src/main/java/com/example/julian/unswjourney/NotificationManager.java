package com.example.julian.unswjourney;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

public class NotificationManager extends AppCompatActivity {
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    private ListView ClassList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notification_manager);
        ClassList = findViewById(R.id.SearchList2);

        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(NotificationManager.this, R.layout.mylist, arrayList);
        ClassList.setAdapter(adapter);
        LoadData();

        ClassList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str = ClassList.getItemAtPosition(position).toString();
                for (Notification element : Background.NotificationList) {
                    if (element.getTitle().equals(str)) {
                        Intent notificationIntent = new Intent(NotificationManager.this, AlertDialog.class);
                        notificationIntent.putExtra("Title", element.getTitle());
                        notificationIntent.putExtra("Body", element.getBody());
                        notificationIntent.putExtra("Key", element.getKey());

                        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(notificationIntent);

                    }
                }
                // Get notification
                // Set Alert with information.
            }

        });
    }

    public void LoadData() {
        for (Notification element : Background.NotificationList) {
            if (!arrayList.contains(element.getTitle())) {
                arrayList.add(element.getTitle());
                adapter.notifyDataSetChanged();
            }
        }


    }


    public void BackNoti(View v) {
        super.onBackPressed();
    }

    public void HelpNoti(View v) {
        Intent intent = new Intent(NotificationManager.this, Chatbot.class);
        startActivity(intent);
    }

    public void HomeNoti(View v) {
        Intent intent = new Intent(NotificationManager.this, MainActivity.class);
        startActivity(intent);
    }

    public void SignOutNoti(View v) {
        FirebaseAuth firebaseAuth;

        //Declaration and defination
        FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {
                    //Do anything here which needs to be done after signout is complete
                    Intent intent = new Intent(NotificationManager.this, Login.class);
                    startActivity(intent);

                } else {
                }
            }
        };
    }
}
