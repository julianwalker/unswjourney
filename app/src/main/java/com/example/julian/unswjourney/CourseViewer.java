package com.example.julian.unswjourney;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

public class CourseViewer extends AppCompatActivity {

    TextView Faculty;
    TextView school;
    TextView hours;
    TextView outline;
    TextView Campus;
    TextView Gened;
    TextView Credit;
    TextView Timetable;
    TextView Terms;
    TextView Career;
    ProgressDialog dialog;
    TextView CourseName;
    TextView CourseDes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
       // getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.course_viewer);
        dialog = new ProgressDialog(this);

        dialog.setMessage("Loading Data..Please Wait");
        dialog.setCancelable(false);
        dialog.show();
        Intent intent = getIntent();
        String Code = intent.getExtras().getString("Name");


        CourseName = findViewById(R.id.coursename);
        CourseDes = findViewById(R.id.coursedescription);

        Faculty = findViewById(R.id.TextFaculty);
         school =  findViewById(R.id.TextSchool); ;
         hours=  findViewById(R.id.TextHours);;
          outline=  findViewById(R.id.TextOutline);
        outline.setClickable(true);
       outline.setMovementMethod(LinkMovementMethod.getInstance());
         Campus=  findViewById(R.id.TextCampus);;
         Gened=  findViewById(R.id.Textgened);;
         Credit =  findViewById(R.id.TextCredit);;
         Timetable=  findViewById(R.id.timetableText);;
        Timetable.setClickable(true);
        Timetable.setMovementMethod(LinkMovementMethod.getInstance());
         Terms=  findViewById(R.id.TextTerm);;
         Career =  findViewById(R.id.CareerText);;


        Load(Code);

    }

    public void Load(String code) {


        JSoupScraper yes = new JSoupScraper();
        Course temp =  yes.GrabHandbookData(code);
        CourseName.setText(code);
        if (temp.getShortdes() != null) { CourseDes.setText(temp.getShortdes()); }else CourseDes.setText("Unavailable");

        if (temp.getFaculty() != null) { Faculty.setText(temp.getFaculty()); }else Faculty.setText("Unavailable");
        if (temp.getSchool() != null) { school.setText(temp.getSchool()); }else school.setText("Unavailable");
        if (temp.getHours() != null) { hours.setText(temp.getHours()); }else hours.setText("Unavailable");
        if (temp.getOutlineurl() != null) { outline.setText(temp.getOutlineurl()); }else outline.setText("Unavailable");
        if (temp.getCampus() != null) { Campus.setText(temp.getCampus()); }else Campus.setText("Unavailable");
        if (temp.getUnitofCredit() != null) { Credit.setText(temp.getUnitofCredit()); }else Credit.setText("Unavailable");
        if (temp.getTimetableurl() != null) { Timetable.setText(temp.getTimetableurl()); }else Timetable.setText("Unavailable");
        if (temp.getOfferingTerm() != null) { Terms.setText(temp.getOfferingTerm()); }else Terms.setText("Unavailable");
        if (temp.getStudyLevel() != null) { Career.setText(temp.getStudyLevel()); }else Career.setText("Unavailable");

        if (temp.getGened() != null) {
            if (temp.getGened()) { Gened.setText("True"); } else { Gened.setText("False"); };
        } else {
            Gened.setText("Unavailable");
        }




dialog.dismiss();

    }

    public void BackView(View v) {
        super.onBackPressed();
    }
    public void HelpClickView(View v) {
        Intent intent = new Intent(CourseViewer.this, Chatbot.class);
        startActivity(intent);
    }

    public void HomeView(View v) {
        Intent intent = new Intent(CourseViewer.this, MainActivity.class);
        startActivity(intent);
    }
    public void SignOutView(View v) {
        FirebaseAuth firebaseAuth;

        //Declaration and defination
        FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null){
                    //Do anything here which needs to be done after signout is complete
                    Intent intent = new Intent(CourseViewer.this, Login.class);
                    startActivity(intent);

                }
                else {
                }
            }
        };

//Init and attach
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.addAuthStateListener(authStateListener);

//Call signOut()
        firebaseAuth.signOut();
    }


}
