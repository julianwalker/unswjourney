package com.example.julian.unswjourney;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class JSoupScraper  {
    public static JSONArray Students3PlusArray;
    public ArrayList<String> HandbookList;
    public ArrayList<String> Timetable;
    public static ProgressDialog dialog;
public static Course TempCourse;
    public ArrayList<String> getHandbookList() {
        return HandbookList;
    }

    public ArrayList<String> getTimetable() {
        return Timetable;
    }
    private Activity activityContext;

    public void JSoupScraper(Activity activityContext)
    {
        this.activityContext = activityContext;
        dialog = new ProgressDialog(activityContext);



    }


    // Add all scraping here!


    // Grab Handbook Data. Store it in a arraylist
//  This returns an array list. Works on a seperate thread. Skrrrrt performance.
    public Course GrabHandbookData(final String CourseCode) {
        Course temp = new Course();

        Thread downloadThread = new Thread() {

            public void run() {

                final StringBuilder builder = new StringBuilder();

                try {
                    Document doc = Jsoup.connect("http://www.handbook.unsw.edu.au/undergraduate/courses/2019/" + CourseCode + ".html").get();

                   Elements UnitofCredit = doc.select("div.a-column.a-column-df-6.a-column-border-left");
                    Elements Description = doc.select("div.a-card-text.m-toggle-text.has-focus");
                   Elements nodeBlogStats = doc.select("div.o-attributes-table-item");
                    Elements Shortdes = doc.select("h2.m-bottom-0");
Elements Gened = doc.select("div.a-column.a-column-df-7.a-column-lg-9.a-column-md-12");
                    String url = doc.select("a.a-btn-secondary.a-btn-secondary--with-icon").first().attr("abs:href");
                temp.setOutlineurl(url);
                   temp.setTimetableurl("http://timetable.unsw.edu.au/2019/"+ CourseCode +".html");
                    temp.setName(CourseCode);

                    for (Element row : Shortdes) {
                        temp.setShortdes(row.text());
                    }
                    for (Element row : Gened) {
                        if (row.text().contains("General Education")) {
                            temp.setGened(true);
                        }
                    }

                    for (Element row : Description) {
                        temp.setDescription(row.text());
                    }
                    for (Element row : UnitofCredit) {
                        temp.setUnitofCredit(row.text());
                    }

                    for (Element row : nodeBlogStats) {
                        String[] words = row.text().split("\\W+");

                        switch (words[0]) {
                            case "Faculty": temp.setFaculty(Join(1, words));
                                break;
                            case "School" : temp.setSchool(Join(1, words));
                                break;
                            case "Study" :  temp.setStudyLevel(Join(2, words));
                                break;
                            case "Offering" :  temp.setOfferingTerm(Join(2, words));
                                break;
                            case "Campus" : temp.setCampus(Join(1, words));
                                break;
                            case "Indicative" : temp.setHours(Join(3, words));
                                break;


                        }


                        }





                } catch (IOException e) {
                    builder.append("Error : ").append(e.getMessage()).append("\n");
                }



            }

        };
        downloadThread.start();
        try {
            downloadThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return temp;

    }



public String Join(int j, String[] strings){

    StringBuffer sb = new StringBuffer("");
    for (int i = j; strings != null && i < strings.length; i++) {
        sb.append(strings[i]);
        if (i < strings.length - 1) {
            sb.append(' ');
        }
    }
    return sb.toString();
}


    // Grab Timetable Information
    // Lmao

    public ArrayList<String> GrabTimetalbeData(final String CourseCode) {
        final ArrayList<String> TempTimeTableList = new ArrayList<String>();
        ExecutorService pool = Executors.newFixedThreadPool(2);
        FutureTask<ArrayList<String>> future =
                new FutureTask(new Callable() {
                    public ArrayList<String> call() {
                         final StringBuilder builder = new StringBuilder();

                        try {
                            Document doc = Jsoup.connect("http://timetable.unsw.edu.au/2018/" + CourseCode + ".html").get();
                            Elements table = doc.select("table");
                            Elements rows = table.select("tr");

                            for (int i = 0; i < rows.size(); i++) {
                                Element row =  rows.get(i);
                                Elements cols = row.select("td");
                                if (cols.text().contains("Tutorial T2") || cols.text().contains("Lecture T2")) {
                                    if (cols.text().length() < 100){
                                       // System.out.println(cols.text());
                                        TempTimeTableList.add(cols.text());
                                        //arrayList.add(cols.text().toString());
                                        builder.append(cols.text());
                                    }

                                }
                            }
                        } catch (IOException e) {
                            builder.append("Error : ").append(e.getMessage()).append("\n");
                        }
                        return TempTimeTableList;
                    }
                });
        pool.execute(future);
        try {
            Timetable = future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return Timetable;


    }


















}
