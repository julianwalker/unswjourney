package com.example.julian.unswjourney;

import android.os.Parcel;
import android.os.Parcelable;

public class Course  {
    String UnitofCredit;
    String Name;
    String Faculty;
    String School;
    String Campus;
    String StudyLevel;
    String OfferingTerm;
    String Hours;
    String Timetableurl;
    String Outlineurl;
    String description;

    public String getShortdes() {
        return Shortdes;
    }

    public void setShortdes(String shortdes) {
        Shortdes = shortdes;
    }

    String Shortdes;

    public Boolean getGened() {
        return gened;
    }

    public void setGened(Boolean gened) {
        this.gened = gened;
    }

    Boolean gened;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnitofCredit() {
        return UnitofCredit;
    }

    public void setUnitofCredit(String unitofCredit) {
        UnitofCredit = unitofCredit;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFaculty() {
        return Faculty;
    }

    public void setFaculty(String faculty) {
        Faculty = faculty;
    }

    public String getSchool() {
        return School;
    }

    public void setSchool(String school) {
        School = school;
    }

    public String getCampus() {
        return Campus;
    }

    public void setCampus(String campus) {
        Campus = campus;
    }

    public String getStudyLevel() {
        return StudyLevel;
    }

    public void setStudyLevel(String studyLevel) {
        StudyLevel = studyLevel;
    }

    public String getOfferingTerm() {
        return OfferingTerm;
    }

    public void setOfferingTerm(String offeringTerm) {
        OfferingTerm = offeringTerm;
    }

    public String getHours() {
        return Hours;
    }

    public void setHours(String hours) {
        Hours = hours;
    }

    public String getTimetableurl() {
        return Timetableurl;
    }

    public void setTimetableurl(String timetableurl) {
        Timetableurl = timetableurl;
    }

    public String getOutlineurl() {
        return Outlineurl;
    }

    public void setOutlineurl(String outlineurl) {
        Outlineurl = outlineurl;
    }


}
